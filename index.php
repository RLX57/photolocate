<?php
require 'vendor/autoload.php';
use \Slim\Slim;
use \Slim\Middleware\CsrfGuard;

//Initialisation des session
session_cache_limiter(false);
session_start();

$app = new Slim();


//Récupération du nom de dossier
$path = dirname($_SERVER['SCRIPT_NAME']);

//Initialisation templates
$loader = new Twig_Loader_Filesystem("templates");
$twig = new Twig_Environment($loader, array('debug' => true));

//Initialisation Eloquent
app\CapsuleManager::bootElo('conf.ini');

//Création des controllers nécessaires
$imgController = new controller\ImagesController();
$partieController = new controller\PartieController();
$adminController = new controller\AdminController();
$villeController = new controller\VilleController();

//Mise en place des chemins

//Accueil
$app->get('/', function () use ($app, $twig, $path, $partieController, $villeController) {
    $tmpl = $twig->loadTemplate('accueil.html.twig');
    $score = $partieController->getMeilleursScores();
    $villes = $villeController->getVilles();
    $tmpl->display(array("path" => $path, 'score' => $score, 'villes' => $villes));
})->name('accueil');

$app->get('/play', function () use ($app, $twig, $path, $villeController) {
	$pseudo = $_GET['pseudo'];
	$ville = $_GET['villeSelectionnee'];
	$difficulte = $_GET['difficulte'];
    $tmpl = $twig->loadTemplate('play.html.twig');
    $tmpl->display(array("path" => $path, "pseudo" => $pseudo, 'ville' => $ville, "difficulte" => $difficulte));
})->name('play');

//Inscription
$app->get('/inscription', function () use ($app, $twig, $path) {
    $tmpl = $twig->loadTemplate('signup.html.twig');
    $tmpl->display(array("path" => $path));
})->name('signup');

$app->post('/inscription', function () use ($app, $twig, $path, $adminController) {
    $tmpl = $twig->loadTemplate('signup.html.twig');
    $tmpl->display(array("path" => $path));

    $pseudo = htmlentities($_POST['pseudosign']);
	$mdp = htmlentities($_POST['mdpsign']);
	$mdp2 = htmlentities($_POST['mdpsign2']);

	if ($mdp == $mdp2){
		$inscription = $adminController->inscription($pseudo,$mdp);
		$app->redirect($path.'/admin/connexion');
	}else{
		echo "<script> $(document).foundation();</script>";
		echo "<script src=".$path."foundation/js/foundation/foundation.alert.js></script>";
		echo "<script src=".$path."foundation/js/vendor/jquery.js></script>";
		echo "<script src=".$path."foundation/js/foundation/foundation.js></script>";
		echo "<div data-alert class='alert-box alert'>";
		echo "Mots de passe differents !!!";
		echo "</div>";
	}
})->name('signupPost');

//Admin
$app->get('/admin', function () use ($app, $twig, $path) {
    $tmpl = $twig->loadTemplate('admin.html.twig');
    $tmpl->display(array("path" => $path));
})->name('admin');

$app->get('/admin/connexion', function () use ($app, $twig, $path) {
    $tmpl = $twig->loadTemplate('login.html.twig');
    $tmpl->display(array("path" => $path));
})->name('login');

$app->post('/admin/connexion', function () use ($app, $twig, $path, $adminController, $path) {
	$pseudo = htmlentities($_POST['pseudo']);
	$mdp = htmlentities($_POST['mdp']);
	$connexion = $adminController->connexion($pseudo,$mdp, $path, $app);

	// redirection si le mot de passe est incorrect
	$tmpl = $twig->loadTemplate('login.html.twig');
    $tmpl->display(array("path" => $path));
})->name('loginPost');

$app->post('/admin', function() use ($app, $twig, $path, $imgController) {
	// Récupération valeurs formulaire 
	$ville = htmlentities($_POST['villeNom']);
	$lat = htmlentities($_POST['lat']);
	$lon = htmlentities($_POST['lon']);
	$maxSize = htmlentities($_POST['MAX_FILE_SIZE']);

	$mess = "";
	
	//Vérification de l'existence du dossier qui contiendra l'image
	if (!file_exists("foundation/img/".$ville)) {
		//Création dans le cas contraire
		mkdir("foundation/img/".$ville, 0777, true);
	}

	//Récupération du dossier cible
	$target_dir = "foundation/img/".$ville."/";
	$target_file = $target_dir . basename($_FILES["imageUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

	//Vérification extension
	if(isset($_POST["submit"])) {
    	$check = getimagesize($_FILES["imageUpload"]["tmp_name"]);
    	if($check !== false) {
        	$uploadOk = 1;
   	 	} else {
        	$mess .=  "Le fichier sélectionné n'est pas une image.";
        	$uploadOk = 0;
    	}
	}
	//Vérification de l'existence de l'image
	if (file_exists($target_file)) {
    	$mess .=  "L'image existe déjà !";
    	$uploadOk = 0;
	}

	//Vérification de la taille
	if ($_FILES["imageUpload"]["size"] > $maxSize) {
    	$mess .=  "Image trop grande.";
    	$uploadOk = 0;
	}
	//Filtre d'extension
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    	$mess .=  "Format d'image incorrect, JPG, JPEG, PNG & GIF uniquement autorisés.";
    	$uploadOk = 0;
	}
	//Vérification de la possibilité d'upload
	if ($uploadOk == 0) {
    	$mess .=  "Erreur, image non uploadée.";
	//Tout est ok, on upload (du moins, on essaie)
	} else {
	    if (move_uploaded_file($_FILES["imageUpload"]["tmp_name"], $target_file)) {
	        $mess .=  "L'image ". basename( $_FILES["imageUpload"]["name"]). " a été uploadée.";
	        //Insertion dans la DB
	        $imgController->insertImage($target_file, $lat, $lon, $ville);

	    } else {
	        $mess .= "Erreur, image non uploadée.";
	    }
	}

	//Affichage message réussite/échec
	$tmpl = $twig->loadTemplate('adminPost.html.twig');
	$urlAccueil = $app->urlFor('accueil');
	$urlAdmin = $app->urlFor('admin');
	$tmpl->display(array("path" => $path, "urlAccueil" => $urlAccueil, "urlAdmin" => $urlAdmin, "message" => $mess));

})->name('uploadImage');

// REST
$app->get('/play/games/:id', function($id) use($app, $twig, $path, $partieController) {
	$app->response->setStatus(200);
    $app->response->headers->set('Content-type','application/json');
    $token = $_GET['token'];
	$partie = $partieController->descriptionPartie($id, $token);
	$json = array("Ville" => $partie[0]->Ville);
	$links = array("rel" => "self", "href" => "/play/games/".$id."?token=".$token);
	$obj = array("partie" => $json, "links" => $links); 
	echo(json_encode($obj, JSON_UNESCAPED_SLASHES));
});

$app->get('/play/games/:id/photos/', function($id) use($app, $twig, $path, $imgController) {
	$app->response->setStatus(200);
	$app->response->headers->set('content-type', 'application/json');
	$token = $app->request()->get('token');
	$images = $imgController->getImagesForPlay($id, $token);
	$links = array('rel' => 'self', 'href' => '/play/games/'.$id.'/photos?token='.$token);
	$obj = array('images' => $images, 'links' => $links);
	echo(json_encode($obj, JSON_UNESCAPED_SLASHES));
});

$app->post('/play/games', function() use($app, $twig, $path, $partieController, $villeController) {
	$app->response->setStatus(201);
	$app->response->headers->set('Content-type', 'application/json');
	$pseudo = htmlentities($_POST['pseudo']);
	$ville = htmlentities($_POST['ville']);
	$diff = $_POST["difficulte"];
	$partie = $partieController->creerPartie($pseudo, $ville, $diff);  
	$lat = $villeController->getLatitude($ville);
	$lon = $villeController->getLongitude($ville);
	$json = array("ID" => $partie->ID, "Token" => $partie->Token, "difficulte" => $partie->Difficulte,  "Ville" => $partie->Ville, "lat" => $lat, "lon" => $lon);
	$links = array('rel' => 'self', 'href' => '/play/games');
	$obj = array('partie' => $json, 'links' => $links);
	echo(json_encode($obj, JSON_UNESCAPED_SLASHES));
});

$app->put('/play/games/:id', function($id) use($app, $twig, $path, $partieController) {
	$app->response->setStatus(200);
	$app->response->headers->set('Content-type', 'application/json');
	$score = $app->request->put('score');
	$token = $app->request->put("token");
	$checkbox = $app->request->put("checkbox");
	$statut = $partieController->enregistrer($id, $token, $score, $checkbox);
	$links = array('rel' => 'self', 'href' => '/play/games/'.$id);
	$obj = array('partie' => $statut, 'links' => $links);
	echo(json_encode($obj, JSON_UNESCAPED_SLASHES));
});

$app->run();