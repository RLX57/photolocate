<?php 

namespace controller;

use modele\Partie as partie;

class PartieController {

	public function descriptionPartie($id, $token) {
		$partie = partie::where('ID', '=', $id)->where('Token' ,'=', $token)->get();
		return $partie;
	}

	public function creerPartie($pseudo, $ville, $difficulte) {
		$partie = new partie();
		$partie->Pseudo = $pseudo;
		$partie->Ville = $ville;
		$partie->Etat = 'En_Cours';
		$partie->Score = 0;
		$partie->Difficulte = $difficulte;

		// Generation token
		$token = uniqid('PL', 'true');
		$partie->Token = $token;

		$partie->save();	
		
		return $partie;	
	}

	public function enregistrer($id, $token, $score, $checkbox) {

		$partie = partie::where('ID', '=', $id)->where('Token', '=', $token)->get();
		if ($checkbox == "true") {
			$partie[0]->Etat = "Enregistree";
		} else {
			$partie[0]->Etat = "Non_Enregistree";
		}

		$partie[0]->Score = $score;
		$partie[0]->save();

		$etat = array("STATUT" => "OK");
		return $etat;
	}

	public function getMeilleursScores() {
		$listeParties = partie::where('Etat', '=', 'Enregistree')->orderBy('Score', 'DESC')->take(15)->get();
		$res = array();

		foreach($listeParties as $partie) {
			$res[] = array('joueur' => $partie->Pseudo, 'score' => $partie->Score);
		}

		return $res;
	}
}