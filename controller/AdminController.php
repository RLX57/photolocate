<?php 

namespace controller;
use \modele\Partie as partie;
use \modele\Images as image;
use \modele\Admin as admin; 

class AdminController {

	public function inscription($pseudo,$mdp) {
		$admin = new admin();
		$admin->Pseudo = $pseudo;

		// cryptage du mdp 
		$salt = uniqid(mt_rand(), true);
		$hash = crypt($mdp, $salt);
		$admin->Mdp = $hash;
		
		$admin->save();
	}

	public function connexion($pseudo,$mdp, $path, $app) {
		
		$user = admin::where('Pseudo', '=', $pseudo)->get();
		$error = false;
		
		if ($user->isEmpty()) {
			$error = true;
		} else {

			$mdpbase = $user[0]->Mdp;

			if (crypt($mdp, $mdpbase) == $mdpbase) {
				$app->redirect($path.'/admin');
			} else {
				$error = true;
			}
		}

		if ($error == true) {
			echo "<script> $(document).foundation();</script>";
			echo "<script src=".$path."foundation/js/foundation/foundation.alert.js></script>";
			echo "<script src=".$path."foundation/js/vendor/jquery.js></script>";
			echo "<script src=".$path."foundation/js/foundation/foundation.js></script>";
			echo "<div data-alert class='alert-box alert'>";
			echo "Pseudo ou mot de passe incorrect !!!";
			echo "</div>";
		}
	}
}