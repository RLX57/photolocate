<?php 

namespace controller;
use \modele\Partie as partie;
use \modele\Images as image;
use \modele\Ville as ville;

class VilleController {

	public function getVilles() {
		$images = new ImagesController();
		$villes = ville::all();
		$res = array();

		foreach($villes as $ville) {
			$img = $images->getOneImage($ville->Nom);
			$res[] = array('nom' => $ville->Nom, 'img' => $img, 'lat' => $ville->Lat_initiale, 'lon' => $ville->Lon_initiale);
		}

		return $res;
	}

	public function getLatitude($ville) {
		$ville = ville::where("Nom", "=", $ville)->get();
		return $ville[0]->Lat_initiale;
	}

	public function getLongitude($ville) {
		$ville = ville::where("Nom", "=", $ville)->get();
		return $ville[0]->Lon_initiale;
	}
}