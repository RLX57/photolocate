<?php 

namespace controller;
use \modele\Partie as partie;
use \modele\Images as image;

class ImagesController {

	public function getImagesForPlay($idPartie, $token) {
		//Récupération de la ville de la partie
		$partie = partie::where('ID', '=', $idPartie)->where('Token', '=', $token)->select('Partie.Ville')->get();

		//Récupérer 10 photos aléatoires associées à la ville de la partie
		$res = array();
		$listeImages = image::where('Ville', '=', $partie[0]->Ville)->orderByRaw('RAND()')->take(10)->get();

		foreach($listeImages as $img) {
			$res[] = array('URL' => $img->URL, 'lat' => $img->Latitude, 'lon' => $img->Longitude, 'ville' => $img->Ville);
		}
		return $res;
	}

	public function insertImage($url, $latitude, $longitude, $ville) {
		$img = new image();
		$img->URL = $url;
		$img->Latitude = $latitude;
		$img->Longitude = $longitude;
		$img->Ville = $ville;
		$img->save();
		//Vérification de bonne insertion à faire
	}

	public function getOneImage($ville) {
		$image = image::where('Ville', '=', $ville)->select('URL')->orderByRaw('RAND()')->take(1)->get();
		$res = "";
		foreach($image as $img) {
			$res = $img->URL;
		}
		return $res;
	}
}