<?php

namespace modele;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class Admin extends Model {

	protected $table = 'Admin';
	protected $primaryKey = 'ID';
	public $timestamps = false;

}