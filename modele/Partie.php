<?php

namespace modele;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class Partie extends Model {

	protected $table = 'Partie';
	protected $primaryKey = 'ID';
	public $timestamps = false;

}