<?php

namespace modele;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class ImagesPartie extends Model {

	protected $table = 'ImagesPartie';
	protected $primaryKey = 'ID';
	public $timestamps = false;

}