<?php

namespace modele;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class Ville extends Model {

	protected $table = 'Ville';
	protected $primaryKey = 'ID';
	public $timestamps = false;

}