<?php

namespace modele;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class Images extends Model {

	protected $table = 'Images';
	protected $primaryKey = 'ID';
	public $timestamps = false;

}