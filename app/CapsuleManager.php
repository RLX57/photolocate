<?php

namespace app;
use Illuminate\Database\Capsule\Manager as DB;

class CapsuleManager {


	public static function bootElo($filename) {
			
		$capsule = new DB;
		$config = parse_ini_file($filename);
		$capsule->addConnection($config);

		$capsule->setAsGlobal();
		$capsule->bootEloquent();

	}
}