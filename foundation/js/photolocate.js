//Game
//Default town
$(document).ready(function(){
	//Check if there is an element with the MAP id so game can be displayed.
	//true: we initialize game.
	//false: we quit.
	if (!$('#gameContainer').length){
		alert("The document is missing an element with gameContainer id ex: <div id='gameContainer'></div>");
		return;
	}else{
		console.log("Document ready");
	}
	//variables
	var game_id, game_token, player_name, town_name, town_lat, town_lon, choixDifficulte, town, turn_index, nb_of_turn, current_answer, game_photos, pointsPartie = 0, distance_pallier_diff_1, map;

	//functions for the app
	var init,getGameInfo, getGameContent, gameStart, updateStats, startTurn, skipTurn, handleClick, calculPoints, displayGame;
	//functions for the prompt
	var clear, write, append;

	(function(){
		$.ajax({
			method: "POST",
			url: "play/games",
			data: 'pseudo='+pseudo+'&difficulte='+difficulte+'&ville='+ville,
			dataType: 'json',
			success: function(jsonData){
				console.log("Step.0: initialization succeed");
				init(jsonData);
			},
			error: function(resultat, statut, erreur){
				console.log("Err.0: unabled to process initial ajax request");
			}
		});
	})();


	//initialize
	

	init = function(jsonObject){
		game_id = jsonObject.partie.ID;
		game_token = jsonObject.partie.Token;
		town_name = jsonObject.partie.Ville;
		town_lat = jsonObject.partie.lat;
		town_lon = jsonObject.partie.lon;
		choixDifficulte = jsonObject.partie.difficulte;
		getGameInfo(game_id,game_token, town_name);
	};

	/*
		@param id: id of the future game
		@param token: token of the current session
	*/
	getGameInfo = function(id, token, ville){
		$.ajax({
			method: "GET",
			url: "play/games/"+id,
			data: 'token='+token,
			success: function(jsonString){
				console.log("Step.1: game info gathered");
				getGameContent(jsonString, id, token);
			},
			error: function(){
				console.log("Err.1: unabled to process game info initialization");
			}
		});
	};

	/*
		initialize town and get the photos to start the game.
		Game start with the callback.
		@param jsonObject: JSON object containing some info
			for the game to start
	*/
	getGameContent = function(jsonString, id, token){
		town = {
			lat : town_lat,
			lon : town_lon,
			name : town_name
		};
		$.ajax({
			method: "GET",
			url: "play/games/"+id+"/photos",
			data: 'token='+token,
			success: function(jsonData){
				console.log("Step.2: game photos gathered");
				gameStart(jsonData);
			},
			error: function(){
				console.log("Err.2: unabled to recover game ressources from server");
			}
		});
	};

	gameStart = function(jsonData){
		console.log("Step.3: please wait, game is starting");
		//Add map and prompt to the DOM
		displayGame();
		//
		game_photos = jsonData;
		//Add default view on default town
		L.mapbox.accessToken = 'pk.eyJ1Ijoicmx4IiwiYSI6IkpDTHlsckUifQ.id22cqkVtiuVBpNy8wDtSw';
		map = L.mapbox.map('map', 'rlx.l6oc4d5a').setView([town_lat, town_lon], 13);

		//Add listener to the div so we can catch clicks
		map.on('click',function(e){

			handleClick(e);
			//L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
		});



		//To move in the start function
		turn_index = 0;
		startTurn(turn_index);
	}

	startTurn = function(turn_index){
		(function(index){
			//Check if we have finished the game
			if (turn_index<10){
				chronoStart();
				//display the image for the current turn
				$("#currentImage").empty();
				$("#currentImage").append('<img src="'+game_photos.images[turn_index].URL+'" alt="image of current turn"/>');
			} else {
				$("#gameContainer").hide();
				$("#finPartie #scoreRealise").html(pointsPartie);
				$("#finPartie").show();

				// recuperation valeur checkbox
				
				// listener sur le bouton enregistrer
				$("#Enregistrer").on("click", function() {
						
					var checkbox;
					if ($('#checkbox').is(':checked')) {
						checkbox = "true";
					} else {
						checkbox = "false";
					}

					$.ajax ({
						method: "PUT",
						url: "play/games/"+game_id,
						data: "score="+pointsPartie+"&token="+game_token+"&checkbox="+checkbox,
						error: function() {
							console.log("Erreur lors de l'enregistrement du score");
						}
					});

					window.setTimeout(function() {
						window.location = path;
					}, 10);
				});
			}
		})(turn_index);
	};

	handleClick = function(mouseEvent){
		//Update the score considering the point
		var lat = mouseEvent.latlng.lat;
		var lng = mouseEvent.latlng.lng;

		// recuperation coordonnees image	
		var imgLat = game_photos.images[turn_index].lat;
		var imgLon = game_photos.images[turn_index].lon;
		var coordImage = L.latLng(imgLat, imgLon);
		var coordClick = L.latLng(lat, lng);

		// recuperation du temps
		var temps = getTime();

		// calcul distance
		var distance = coordClick.distanceTo(coordImage);
		// appel calcul points
		calculPoints(distance, temps)


		//go to next turn
		turn_index++;
		startTurn(turn_index);
	};

	calculPoints = function(distance, temps) {
		var nbPoints = 0;
		switch (choixDifficulte) {
			case "1" : 
				distance_pallier_diff_1 = 150;
				break;
			case "2" : 
				distance_pallier_diff_1 = 100;
				break;
			case "3" : 	
				distance_pallier_diff_1 = 75;
				break;
			default : 
				distance_pallier_diff_1 = 100;
				break;
			}

		if(distance <= distance_pallier_diff_1) {
			nbPoints += 5;
		}
		else {
			if(distance <= 2*distance_pallier_diff_1) {
				nbPoints += 3;
			}
			else {
				if(distance <= 3*distance_pallier_diff_1) {
					nbPoints += 1;
				}
				else {
					nbPoints += 0;
				}
			}
		}
		
		if(temps <= 2) {
			nbPoints *= 4;
		}
		else {
			if(temps <= 5) {
				nbPoints *= 2;
			}
			else {
				if(temps > 10) {
					nbPoints *= 0;
				}
			}
		}

		pointsPartie += nbPoints;
		document.getElementById("scoreActuel").innerHTML = pointsPartie;
		document.getElementById("scoreTour").innerHTML = nbPoints;
		return nbPoints;
	};


	displayGame = function(){
		//$("#gameContainer").append('<div id="map></div>').append('<div id="gamePrompt"></div>').append('<div id="gameImage"></div>');
		$("#map").show();
		$("#gameImage").show();
		$("#chronotime").show();
		$("#gameContainer").height("550px");
	};
	
	var reponse = 0;

	correction = function() {
		
		/*var map2 = L.map('map2').setView([town_lat, town_lon], 13);
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map2);
*/
		if (reponse === 0) {
			L.mapbox.accessToken = 'pk.eyJ1Ijoicmx4IiwiYSI6IkpDTHlsckUifQ.id22cqkVtiuVBpNy8wDtSw';
			var map2 = L.mapbox.map('map2', 'rlx.l73fd66k').setView([town_lat, town_lon], 13);
			
			reponse++;
			var imgLat;
			var imgLng;

			$("#map2").show();

			for (var j=0; j < game_photos.images.length; j++) {
				imgLat = game_photos.images[j].lat;
				imgLng = game_photos.images[j].lon;
				L.marker([imgLat, imgLng]).addTo(map2).bindPopup("<img style='max-width:200px;max-height:200px' src="+game_photos.images[j].URL+"></img>");

			}
		} else {
			$("#map2").toggle();
		}
	}

	/*
		Functions for the prompt
	*/

	clear = function(){
		$("#gamePrompt").empty();
	};
	append = function(texte){
		$("#gamePrompt").append(texte);
	};
	write = function(texte){
		clear();
		append(texte);
	};

	// Chrono

	var startTime = 0;
	var start = 0;
	var end = 0;
	var diff = 10;
	var timerID = 0;

	chrono = function(){
		end = new Date(); 
		diff = end - start;
		diff = new Date(diff);

		var sec = 10 - diff.getSeconds();
		
		if (sec != -1) {
			document.getElementById("time").innerHTML = sec;
			timerID = setTimeout("chrono()", 1000);
		}

	};

	chronoStart = function(){
		start = new Date();
		chrono();
	};

	getTime = function() {
		var time = new Date(diff);
		return time.getSeconds();
	};

});
